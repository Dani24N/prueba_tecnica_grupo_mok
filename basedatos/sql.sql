CREATE DATABASE pruebaMok;

USE pruebaMok;

CREATE TABLE personas(
id_persona int NOT NULL AUTO_INCREMENT,
nombre varchar(255) NOT NULL,
apellido varchar(255) NOT NULL,
edad int NOT NULL,
sexo varchar(20) NOT NULL,
correo varchar(255) NOT NULL,
PRIMARY KEY(id_persona)
);

INSERT INTO personas (id_persona,nombre,apellido,edad,sexo,correo) VALUES (1,'Janis','Bell',23,'F','janis@gmail.com'),(2,'Frank','Miles',19,'M','frank@gmail.com'),(3,'Clifton', 'Crawford', 56,'M', 'clifton@gmail.com '),(4,'Cesar', 'Mckinney', 32,'M', 'cesar@gmail.com'),
(5,'Lorene',' Dixon',78,'F', 'lorene@gmail.com'),(6,'Tommy','Pope', 65,'M', 'tommy@gmail.com'),(7,'Rosemary','Jackson',95,'F','rosemary@gmail.com'),(8,'Willie','Dennis', 21,'M','willie@gmail.com'),(9,'Winifred','Carroll',23,'M','winifred@gmail.com'),(10,'Cynthia','Perkins',71,'F','cynthia@gmail.com');