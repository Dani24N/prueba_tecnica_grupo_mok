<?php

namespace persistencia\vo;

class Persona {

    private $id_persona;
    private $nombre;
    private $apellido;
    private $edad;
    private $sexo;
    private $correo;

    public function setIdPersona($id_persona){
        $this->id_persona = $id_persona;
    }
    public function getIdPersona(){
        return $this->id_persona;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }    

    public function setApellido($apellido){
        $this->apellido = $apellido;
    }
    public function getApellido(){
        return $this->apellido;
    }

    public function setEdad($edad){
        $this->edad = $edad;
    }
    public function getEdad(){
        return $this->edad;
    }

    public function setSexo($sexo){
        $this->sexo = $sexo;
    }
    public function getSexo(){
        return $this->sexo;
    }

    public function setCorreo($correo){
        $this->correo = $correo;
    }
    public function getCorreo(){
        return $this->correo;
    }
    
}