<?php

namespace persistencia\dao;

use PDO;
use persistencia\vo\Persona;

class PersonasDAO{
    
    private $cnn;
    private $personas;

    public function __construct($cnn) {
        $this->cnn = $cnn;
        $this->persona = new Persona(); 
    }

    public function mostrarPersonas(){
        $query = 'SELECT * FROM personas';
        $consultarPersonas = $this->cnn->prepare($query);
        $consultarPersonas->execute();
        $consulta = $consultarPersonas->fetchAll(PDO::FETCH_CLASS);
        return $consulta;
    }

    public function insertarPersona(Persona $persona){
        $query = "INSERT INTO personas (nombre,apellido,edad,sexo,correo) VALUES (:nom,:ape,:edad,:sexo,:correo)";
        $insertar = $this->cnn->prepare($query);
        $insertar->bindValue(':nom',$persona->getNombre());
        $insertar->bindValue(':ape',$persona->getApellido());
        $insertar->bindValue(':edad',$persona->getEdad());
        $insertar->bindValue(':sexo',$persona->getSexo());
        $insertar->bindValue(':correo',$persona->getCorreo());
        $insertar->execute();
    }

    public function mostrarPersona(Persona $persona){
        $query = "SELECT * FROM personas WHERE id_persona = :id";
        $consultar = $this->cnn->prepare($query);
        $consultar->bindValue(':id',$persona->getIdPersona());
        $consultar->execute();
        $resultado = $consultar->fetchAll(PDO::FETCH_CLASS);
        return $resultado;
    }

    public function editarPersona(Persona $persona){  
        $query="UPDATE personas SET nombre=:nom,apellido=:ape,edad=:edad,sexo=:sexo,correo=:correo WHERE id_persona = :id ";
        $editar = $this->cnn->prepare($query);
        $editar->bindValue(':nom',$persona->getNombre());
        $editar->bindValue(':ape',$persona->getApellido());
        $editar->bindValue(':edad',$persona->getEdad());
        $editar->bindValue(':sexo',$persona->getSexo());
        $editar->bindValue(':correo',$persona->getCorreo());
        $editar->bindValue(':id',$persona->getIdPersona());
        $editar->execute();
    }

    public function borrarPersona(Persona $persona){
        $query = "DELETE FROM personas WHERE id_persona = :id";
        $eliminar = $this->cnn->prepare($query);
        $eliminar->bindValue(':id',$persona->getIdPersona());
        $eliminar->execute();
    }
}
