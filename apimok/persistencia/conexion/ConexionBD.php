<?php

namespace persistencia\conexion;
use PDO;
use PDOException;

class ConexionBD {

    public static function conectarBD(){
        try{
            $cnn = new PDO('mysql:host=localhost;dbname=pruebaMok','root','');
            return $cnn;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

} 