<?php 

$proyecto = '/pruebatecnica/apimok/index.php';
require_once './autoload.php';
require_once './rutas.php';
if(!isset($_SERVER['PATH_INFO'])){
    return;
}
use persistencia\conexion\ConexionBD;

$cnn = ConexionBD::conectarBD();
$url = $proyecto.$_SERVER['PATH_INFO'];

foreach(get_defined_constants() as $constante){
    if(!is_array($constante)){
        continue;
    }
    if($url == $constante['url']){
        $controlador = '\\controlador\\'.$constante['controlador'];
        $obj = new $controlador($cnn);
        $metodo = $constante['metodo'];
        $obj->$metodo();
    break;
    }
}
