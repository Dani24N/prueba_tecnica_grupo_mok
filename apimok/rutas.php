<?php
define("CONSULTAR",array('controlador'=>'PersonasControlador','metodo'=>'consultarPersonas','url'=>$proyecto.'/consultar/personas'));
define("INSERTAR",array('controlador'=>'PersonasControlador','metodo'=>'registrarPersonas','url'=>$proyecto.'/crear/persona'));
define("EDITAR",array('controlador'=>'PersonasControlador','metodo'=>'modificarPersona','url'=>$proyecto.'/editar/persona'));
define("VER",array('controlador'=>'PersonasControlador','metodo'=>'verPersona','url'=>$proyecto.'/ver/persona'));
define("ELIMINAR",array('controlador'=>'PersonasControlador','metodo'=>'eliminarPersona','url'=>$proyecto.'/eliminar/persona'));
