<?php

namespace controlador;

use persistencia\dao\PersonasDAO;
use persistencia\vo\Persona;
header('Content-type: application/json');
header('Access-Control-Allow-Origin:*');
class PersonasControlador{

    private $personasDAO;
    public function __construct($cnn)
    {
        $this->personasDAO = new PersonasDAO($cnn) ;
    }

    public function consultarPersonas(){
       $consulta = $this->personasDAO->mostrarPersonas();
       echo json_encode($consulta);
    }
    public function registrarPersonas(){
        $persona = new Persona();
        $persona->setNombre($_POST['nombre']);
        $persona->setApellido($_POST['apellido']);
        $persona->setEdad($_POST['edad']);
        $persona->setSexo($_POST['sexo']);
        $persona->setCorreo($_POST['correo']);
        $this->personasDAO->insertarPersona($persona);
    }

    public function modificarPersona(){
        $persona = new Persona();
        $persona->setIdPersona($_POST['id']);
        $persona->setNombre($_POST['nombre']);
        $persona->setApellido($_POST['apellido']);
        $persona->setEdad($_POST['edad']);
        $persona->setSexo($_POST['sexo']);
        $persona->setCorreo($_POST['correo']);
        $this->personasDAO->editarPersona($persona);
    }
    
    public function verPersona(){
        $persona = new Persona();
        $persona->setIdPersona($_POST['id']);
        $resultado = $this->personasDAO->mostrarPersona($persona);
        echo json_encode($resultado);
    }
    public function eliminarPersona(){
        $persona = new Persona();
        $persona->setIdPersona($_POST['id']);
        $resultado = $this->personasDAO->borrarPersona($persona);
        echo json_encode($resultado);
    }
}
