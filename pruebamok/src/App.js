import React from 'react';
import {BrowserRouter ,Switch, Route} from 'react-router-dom';
import Menu from './componentes/Menu';
//import GesPersonas from './componentes/GesPersonas';
import Personas from './paginas/MostrarPersonas';
import CrearPersona from './paginas/CrearPersona';
import EditarPersona from './paginas/EditarPersona';
function App() {
  return (
      <BrowserRouter>
        <Menu>
          <Switch>
            <Route exact path='/' component={Personas}/>          
            <Route exact path='/crear/persona' component={CrearPersona}/>
            <Route exact path='/ver/persona/:persona' component={EditarPersona}/>
          </Switch>
        </Menu>
      </BrowserRouter>
  );
}

export default App;
