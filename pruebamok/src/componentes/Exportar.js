import React from 'react';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

export const Exportar = (props) => {

    const tipoArchivo = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const extencion = '.xlsx';

    const exportarExcel = (datos, nombre) => {
        const hojadetrabajo = XLSX.utils.json_to_sheet(datos);
        const librotrabajo = { Sheets: { 'data': hojadetrabajo }, SheetNames: ['data'] };
        const datosArchivo = XLSX.write(librotrabajo, { bookType: 'xlsx', type: 'array' });
        const data = new Blob ([datosArchivo], {type: tipoArchivo});
        FileSaver.saveAs(data, nombre + extencion);
    }

    return (
        <button className="btn btn-outline-primary" onClick={(e) => exportarExcel(props.datos,props.nombre)}>Exportar xlsx</button>
    )
}