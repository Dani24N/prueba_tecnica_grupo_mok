import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Menu extends Component{
    render(){
        return(
            <div className="container">
                <nav className="navbar bg-light">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                        <Link className="nav-link" to="/">Gestionar Personas</Link>
                        </li>
                    </ul>
                </nav>
                <div className="container-lg">
                    <div className="row">
                    <div className="col-lg-12">
                    {this.props.children}
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}