import React from 'react';
import { Link } from 'react-router-dom';

function ListaPersonas(props){
    const listaPersonas = props.lista;
    return(
        <React.Fragment>
            <table className="table table-bordered table-striped table-hover text-center">
                <thead className="thead-dark ">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Edad</th>
                        <th>Sexo</th>                        
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
            {listaPersonas.map(persona=>{
               return (
                <tr key={persona.id_persona}>
                    <td>{persona.nombre}</td>
                    <td>{persona.apellido}</td>
                    <td>{persona.edad}</td>
                    <td>{persona.sexo}</td>
                    <td>{persona.correo}</td>
                    <td>
                        <Link className="btn btn-info" to={`/ver/persona/${persona.id_persona}`}>Editar</Link>
                        &nbsp;
                        <button className="btn btn-sm btn-warning" onClick={props.onClick} value={persona.id_persona}>Eliminar</button>
                    </td>
                </tr>);
            })}
            </tbody>
            </table>
        </React.Fragment>
    );
}

export default ListaPersonas;