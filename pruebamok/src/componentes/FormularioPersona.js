import React,{ Component } from "react";
class FormularioPersona extends Component{
    state = {
        nombreError:null,
        apellidoError:null,
        edadError:null,
        sexoError:null,
        correoError:null,
        pase:true,
    }
    prueba = ()=>{
        let nombreError = '';
        let apellidoError = '';
        let edadError = '';
        let sexoError = '';
        let correoError = '';
        let pase = '';
        
        if(!this.props.value.nombre){
            nombreError='Nombre esta vacio!';
        }
        if(!this.props.value.apellido){
            apellidoError='Apellido esta vacio!';
        }
        if(!this.props.value.edad){
            edadError='Edad esta vacio!';
        }
        if(!this.props.value.sexo){
            sexoError='Seleciona tu genero!';
        }
        if(!this.props.value.correo){
            correoError='Correo esta vacio!';
        }
        if(nombreError || apellidoError || edadError || sexoError || correoError){
            this.setState({nombreError,apellidoError,edadError,sexoError,correoError,});
            pase = false;
        }
        if(pase === ''){
            this.props.click();
        }
    }
    render(){
        return(
            <div className="form-group row justify-content-center lg-col-12">
                <form>
                    <div className="form-group">
                        <label className="text-center" htmlFor="">Nombre</label>
                        <input className="form-control" type="text" onChange={this.props.onChange} name="nombre" value={this.props.value.nombre || ''}/>
                        <small className="form-text text-danger">{this.state.nombreError}</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Apellido</label>
                        <input className="form-control" type="text" onChange={this.props.onChange} name="apellido" value={this.props.value.apellido || ''}/>
                        <small className="form-text text-danger">{this.state.apellidoError}</small>

                    </div>
                    <div className="form-group">
                        <label htmlFor="">Edad</label>
                        <input className="form-control" type="number" onChange={this.props.onChange} name="edad" value={this.props.value.edad || ''}/>
                        <small className="form-text text-danger">{this.state.edadError}</small>

                    </div>
                    <div className="form-group">
                        <label htmlFor="">Sexo</label>
                        <select className="form-control" name="sexo" value={this.props.value.sexo} onChange={this.props.onChange}>
                            <option value="">Seleccione</option>
                            <option value="F">Femenina</option>
                            <option value="M">Masculino</option>
                        </select>
                        <small className="form-text text-danger">{this.state.sexoError}</small>

                    </div>
                    <div className="form-group">
                        <label htmlFor="">Correo</label>
                        <input className="form-control" type="email" onChange={this.props.onChange} name="correo" value={this.props.value.correo || ''}/>
                        <small className="form-text text-danger">{this.state.correoError}</small>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success btn-block" type='button' onClick={this.prueba}>Registrar</button>
                    </div>
                </form>
            </div>
        );
    }
}
export default FormularioPersona;