import React , {Component} from 'react';
import FormularioPersona from '../componentes/FormularioPersona';
import axios from 'axios';
import swal from 'sweetalert';
class CrearPresona extends Component {
    state={
        data:{
            nombre:'',
            apellido:'',
            edad:'',
            sexo:'',
            correo:'',
        },
        exitoso:false,
        fallo:false,
        titulo:'',
        message:'',
    }
    handleSubmit = () =>{
        let datos = new FormData();
        datos.append('nombre',this.state.data.nombre);
        datos.append('apellido',this.state.data.apellido);
        datos.append('edad',this.state.data.edad);
        datos.append('sexo',this.state.data.sexo);
        datos.append('correo',this.state.data.correo);
        this.insertar(datos);
    }
    insertar = (datos) =>{
        axios.post('http://localhost:8080/pruebatecnica/apimok/index.php/crear/persona',datos).then(succ=>{
            this.setState({exitoso:true,titulo:'Bien',message:'Se registrado correctamente'});
            swal({
                icon:"success",
                show:this.state.exitoso,
                title:this.state.titulo,
                text:this.state.message,
                onConfirm:()=>{this.setState({exitoso:false})}
            });
            window.location.replace('http://localhost:3000/');
    }).catch(err =>{
            this.setState({fallo:true,titulo:'Error',message:err.statusText});
            swal({
                icon:"error",
                show:this.state.fallo,
                title:this.state.titulo,
                text:this.state.message,
                onConfirm:()=>{this.setState({fallo:false})}
            });
        })
    }
    handleChange = (e) =>{
        this.setState({
            data:{
                ...this.state.data,
                [e.target.name] : e.target.value,
            }
        });
    }
    render(){
        return(
            <React.Fragment>
            <FormularioPersona
                value={this.state.data}
                onChange={this.handleChange}
                click={this.handleSubmit}
             />
             </React.Fragment>
        );
    }
}

export default CrearPresona;