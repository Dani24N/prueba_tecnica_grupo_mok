import React,{ Component} from "react";
import Axios from "axios";
import FormularioPersona from '../componentes/FormularioPersona';
import swal from 'sweetalert';
class EditarPersona extends Component {
    state={
        form:undefined,
        carga:true,
        exitoso:false,
        fallo:false,
        titulo:'',
        message:'',

    };
    componentDidMount(){
       this.consultaPersona();
    }
    consultaPersona = ()=>{
        let id = new FormData();
        id.append('id',this.props.match.params.persona);
        Axios.post('http://localhost:8080/pruebatecnica/apimok/index.php/ver/persona',id).then(succ=>{
            this.setState({carga:false,form:succ.data[0]});
        });
    }
    handleChange = (e) =>{
        this.setState({form:{
            ...this.state.form,
            [e.target.name]:e.target.value,
        }});
    }
    handleClick = ()=>{
        let form = new FormData();
        form.append('id',this.props.match.params.persona);
        form.append('nombre',this.state.form.nombre);
        form.append('apellido',this.state.form.apellido);
        form.append('edad',this.state.form.edad);
        form.append('sexo',this.state.form.sexo);
        form.append('correo',this.state.form.correo);
        Axios.post('http://localhost:8080/pruebatecnica/apimok/index.php/editar/persona',form).then(succ=>{
            this.setState({exitoso:true,titulo:'Bien',message:'Se Editado correctamente'});
            swal({
                icon:"success",
                show:this.state.exitoso,
                title:this.state.titulo,
                text:this.state.message,
                onConfirm:()=>{this.setState({exitoso:false})}
            });
            window.location.replace('http://localhost:3000/');
        }).catch(err=>{
            this.setState({fallo:true,titulo:'Error',message:err.statusText});
            swal({
                icon:"error",
                show:this.state.fallo,
                title:this.state.titulo,
                text:this.state.message,
                onConfirm:()=>{this.setState({fallo:false})}
            });
        });
    }
    render(){
        if(this.state.carga){
            return(<div>Cargando.....</div>);
        }
        return(
            <div>
                <FormularioPersona
                    value={this.state.form}
                    onChange={this.handleChange}
                    click={this.handleClick}
                />
            </div>
        );
    }
}
export default EditarPersona;
 