import React,{ Component } from "react";
import Axios from 'axios';
import ListaPersonas from '../componentes/ListaPersonas';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';
import {Exportar} from '../componentes/Exportar';
class Personas extends Component{
    state = {
        loading:true,
        data:undefined,
        resultado:undefined,
        confirmar:false,
        mostrar:false,
    };
    componentDidMount(){
        this.consultaPersonas();
    }
    consultaPersonas = () =>{
        try{
         Axios.get('http://localhost:8080/pruebatecnica/apimok/index.php/consultar/personas')
            .then(res => {
                this.setState({loading:false,data:res.data,resultado:res.data});
            });
        }catch(erro){
            console.log(erro);
        }
    }
    handleClick=(e)=>{
        this.setState({mostrar:true});
        let data = e.target.value;
        swal({
            show:this.state.mostrar,
            title: "Estas segur@?",
            text: "Deseas eliminar esta persona?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((eliminar) => {
            if (eliminar) {
                let id = new FormData();
                id.append('id',data);
                Axios.post('http://localhost:8080/apimok/index.php/eliminar/persona',id).then(succ=>{
                    window.location.reload(false);
                });
            }
          });
    }

    filtro = (e)=>{
        const buscar = this.state.data;
        const resultado = buscar.filter(data => {
            return `${data.sexo} ${data.edad}`.toLowerCase().match(e.target.value.toLowerCase());
        }
        );
        //this.state.resultado = resultado;
        this.setState({resultado:resultado},()=>{console.log(this.state.resultado)});
        if(e.target.value === ''){
            this.setState({resultado:buscar},);
        }
}
    render(){
        
        if(this.state.loading){
            return(
            <div className="row justify-content-center">Cargando.....</div>
            );
        }
        return(
            <div>
                <div className="form-group"><label>Filtro:</label><input className="form-control" type="text" placeholder="Sexo, edad..." onChange={e => this.filtro(e)}/></div>
                <div className="container">
                    <div className="row justify-content-center">
                    <div className="col-sm-4">
                        <Link className="btn btn-success btn-block justify-content-center" to="/crear/persona">Crear Persona</Link> &nbsp;
                    </div>
                    <div className="col-sm-4 justify-content-center">
                        <Exportar datos={this.state.resultado} nombre={"personas"}/>
                    </div>
                    </div>
                </div>
                <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-12">
                       <ListaPersonas 
                            lista={this.state.resultado}
                            onClick={this.handleClick}
                        />
                    </div>
                </div>
                </div>
            </div>
        );
    }

}

export default Personas;